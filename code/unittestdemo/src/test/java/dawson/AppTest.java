package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
   

    @Test
    public void shouldReturn5()
    {
        assertEquals( "Should return 5", App.echo(5), 5);
    }

    @Test
    public void  shouldReturn6()
    {
        assertEquals( "Should return 6", App.echo(6), 5);
    }
    
}
